﻿using System;

static class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("What is your name?");
        
        string? name = Console.ReadLine();

        if(name?.Length > 0)
        {
            Console.WriteLine($"Hello, {name}!!!");
        }
    }
}